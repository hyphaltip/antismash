import unittest

from antismash.specific_modules.nrpspks.nrpspksdomainalign import plot_domain_align as pda


class TestPlotDomainAlign(unittest.TestCase):
    def test_cleanup_unique_phylo_clade(self):
        """Test plot_domain_align.cleanup_unique_phylo_clade"""
        phylo_clade = ['Fake_1', 'II_c5_g1', 'Fake_2', 'II_c5_g2', 'Fake_3']
        expected = ['Fake_1', 'II_c5', 'Fake_2', 'Fake_3']
        pda.cleanup_unique_phylo_clade(phylo_clade)
        self.assertEqual(expected, phylo_clade)

    def test_extract_unique_phylo_clade(self):
        """Test plot_domain_align.extract_unique_phylo_clade"""
        ks_anno = {
            'a': ['noInfo0', 'noInfo1', 'noInfo2', 'noInfo3', 'noInfo4', 'noInfo5', 'noInfo6', 'noInfo7', 'III_d', 'noInfo8', 'noInfo9'],
            'b': ['noInfo0', 'noInfo1', 'II_g', 'XI_b', 'VI_f', 'II_c3', 'III_h', 'II_c3', 'VIII', 'II_c3', 'XI_b', 'II_c3', 'III_d'],
            'c': ['III_e', 'II_c5', 'III_d'],
            'd': ['noInfo0', 'III_d', 'III_a'],
            'e': ['noInfo0', 'IV_b', 'XI_c', 'XI_a', 'XVIII', 'noInfo1', 'II_d', 'VI_a', 'III_c', 'III_c', 'II_c2', 'XVIII', 'II_c5', 'XI_b', 'II_d', 'XVIII', 'noInfo2', 'III_d'],
            'f': ['XI_a', 'II_c5', 'III_d'],
            'g': ['II_c3', 'III_d', 'noInfo0'],
            'h': ['XI_b', 'II_c3', 'III_d'],
            'i': ['out_group_like', 'out_group_like', 'out_group_like', 'out_group_like', 'out_group_like', 'out_group_like', 'out_group_like', 'out_group_like', 'out_group_like', 'out_group_like', 'III_d'],
            'j': ['II_c3', 'III_d', 'III_a'],
            'k': ['noInfo0', 'noInfo1', 'noInfo2', 'noInfo3', 'noInfo4', 'noInfo5', 'noInfo6', 'noInfo7', 'III_d']
        }

        expected = ['III_d', 'II_g', 'XI_b', 'VI_f', 'II_c3', 'III_h', 'III_e', 'III_a', 'IV_b', 'XI_c', 'XI_a',
                    'XVIII', 'II_d', 'VI_a', 'III_c', 'II_c5', 'II_c2', 'VIII']

        result = pda.extract_unique_phylo_clade(ks_anno)

        # lists are unsorted, so sort them before comparing
        expected.sort()
        result.sort()
        self.assertEqual(expected, result)
